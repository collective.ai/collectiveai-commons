import logging

from time import sleep
from pymongo import MongoClient
from typing import List, Dict


logger = logging.getLogger(__name__)


class MongoDriver():
    """Class to manage Mongo connections"""

    def __init__(self, mongo_uri, db_name, collection_name):
        """

        :param mongo_uri:
        :param db_name:
        :param collection_name:
        """
        self.client = self.get_client(mongo_uri)
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]
        self.check_connection()

    def get_client(self, mongo_uri: str) -> MongoClient:
        """
        Get client connection

        :param mongo_uri:
        """

        try:
            client = MongoClient(mongo_uri)
            return client
        except Exception as err:
            logger.error(f'mongo_driver => {err}')
            logger.info('mongodb not ready, waiting...')
            sleep(20)
        return self.get_client(mongo_uri)

    def check_connection(self) -> None:
        """
        Check connection
        """

        server_info = self.client.server_info()
        logger.info(f'server_info => {server_info["version"]}')

    def get_syntax_info(self, _id: str) -> List[Dict[str, str]]:
        """
        Get syntax info

        :param _id:
        """

        query = {
            '_id': _id
        }
        result = self.collection.find_one(query)
        return result

    def save_syntax_info(self, doc: Dict[str, str]):
        """
        Save the syntax info

        :param doc:
        """

        filter = {
            '_id': doc['_id']
        }
        self.collection.update(filter, doc, upsert=True)
