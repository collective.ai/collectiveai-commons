import os

from glob import glob
from typing import List
from distutils.core import run_setup
from itertools import chain


SRC_PATH = os.getenv('SRC_PATH', '/tmp/src')
OUT_FILE = os.getenv('OUT_FILE', '/tmp/setup-requirements.txt')


def get_requirements(setup_path: str) -> List[str]:
    result = run_setup(setup_path, stop_after='init')
    requirements = result.install_requires
    return requirements


def main():
    setup_files = glob(f'{SRC_PATH}/**/setup.py')
    requirements = map(get_requirements, setup_files)
    requirements = chain(*requirements)
    with open(OUT_FILE, 'w') as f:
        f.write('\n'.join(requirements))


if __name__ == '__main__':
    main()
