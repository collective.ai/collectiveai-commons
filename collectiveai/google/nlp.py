from google.cloud import language_v1
from google.cloud.language_v1 import enums
from typing import List, Dict, Any


client = language_v1.LanguageServiceClient()
encoding_type = enums.EncodingType.UTF8
type_ = enums.Document.Type.PLAIN_TEXT


def get_syntax_data(token: Any) -> Dict[str, str]:
    """
    Get the syntax data of token

    :param token: the token
    """

    pos = str(token.part_of_speech)
    syntax_info = pos.split('\n')
    syntax_info = filter(bool, syntax_info)
    syntax_info = {x.split(': ')[0]: x.split(': ')[1] for x in syntax_info}
    syntax_info['lemma'] = str(token.lemma)
    return syntax_info


def analyze_syntax(
        sentence: str,
        lang: str = 'es') -> List[Dict[str, str]]:
    """
    Analyze the syntax of a list of sentences

    :param sentence: list of sentences
    :param lang: optional you could pass a language, default is 'es'
    """

    document = {'content': sentence, 'type': type_, 'language': lang}
    response = client.analyze_syntax(document, encoding_type=encoding_type)
    syntax_data = map(get_syntax_data, response.tokens)
    syntax_data = list(syntax_data)
    return syntax_data
