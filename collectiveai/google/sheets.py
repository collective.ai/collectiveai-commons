import logging
import pickle
import gspread

import pandas as pd

from os import path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from gspread_formatting import cellFormat, format_cell_range, textFormat, color
from typing import Any


logger = logging.getLogger(__name__)


SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/drive'
]
CEREDENTIALS_FILE = '/resources/google-credentials/credentials.json'
TOKEN_PICKLE = '/resources/google-credentials/token.pickle'


def gsheet_api_check(
        cred_file: str = None,
        token_file: str = None):
    """
    Generate credentials object,
    for creating cred file follow the following link:
    https://developers.google.com/sheets/api/quickstart/python

    :param cred_file: path to credential file
    :param token_file: path to token file
    """

    cred_file = cred_file or CEREDENTIALS_FILE
    token_file = token_file or TOKEN_PICKLE
    creds = None
    if path.exists(token_file):
        with open(token_file, 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                cred_file,
                SCOPES
            )
            creds = flow.run_local_server(port=0)
        with open(token_file, 'wb') as token:
            pickle.dump(creds, token)
    return creds


def pull_sheet_data(
        spreadsheet_id: str,
        range_name: str,
        creds: Any = None):
    """
    Writes the df to an already created gsheet

    :param spreadsheet_id: the id of the gsheet
    :param range_name:
    :param creds: google credentias object
    """

    creds = creds or gsheet_api_check()
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    result = sheet.values().get(
        spreadsheetId=spreadsheet_id,
        range=range_name
    )
    result = result.execute()
    values = result.get('values', [])

    if not values:
        logger.error('no data found')
        return

    rows = sheet.values().get(
        spreadsheetId=spreadsheet_id,
        range=range_name
    )
    rows = rows.execute()
    data = rows.get('values')
    logger.info('data copied')
    return data


def push_sheet_data(
        spreadsheet_id: str,
        data: pd.DataFrame,
        sheetname: str,
        creds: Any = None):
    """
    Writes the df to an already created gsheet

    :param spreadsheet_id: the id of the gsheet
    :param data: data frame to be pushed
    :param sheetname: destination sheetname
    :param creds: google credentias object
    """

    creds = creds or gsheet_api_check()
    gc = gspread.authorize(creds)

    sheet = gc.open_by_key(spreadsheet_id)

    # delete sheet
    try:
        worksheet = sheet.worksheet(sheetname)
        sheet.del_worksheet(worksheet)
    except gspread.models.WorksheetNotFound:
        pass

    # create sheet with data shape
    nrows, ncols = data.shape
    sheet.add_worksheet(title=sheetname, rows=nrows, cols=ncols+1)

    # get the instance of the second sheet
    worksheet = sheet.worksheet(sheetname)

    all_data = data.values.tolist()
    all_data.insert(0, list(data.columns))  
    worksheet.insert_rows(all_data)


def create_sheet(
        spreadsheet_name: str,
        spreadsheet_folder: str = None,
        creds: Any = None):
    """
    Create a spreadsheet in a drive folder

    :param spreadsheet_name: nema for spreadsheet
    :param spreadsheet_folder: spreadsheet_folder, option
    :param creds: google credentias object
    """

    creds = creds or gsheet_api_check()
    gc = gspread.authorize(creds)
    return gc.create(spreadsheet_name, spreadsheet_folder)


def delete_worksheet(spreadsheet: object, sheet_name: str):
    """
    Deletes the sheet from the spreadsheet

    :param spreadsheet: spreadsheet object
    :param sheet_name: sheet name to delete
    """

    spreadsheet.del_worksheet(spreadsheet.worksheet(sheet_name))


def format_cell(
        worksheet,
        range: str,
        background_color: color,
        foreground_color: color,
        bold: bool = False,
        horizontal_alignment: str = 'CENTER'):
    """
    Format cell

    :param spreadsheet: spreadsheet object
    :param sheet_name: sheet name to delete
    """

    fmt = cellFormat(
        backgroundColor=background_color,
        textFormat=textFormat(bold=bold, foregroundColor=foreground_color),
        horizontalAlignment=horizontal_alignment
    )

    format_cell_range(worksheet, range, fmt)
