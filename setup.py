import os
from setuptools import find_packages, setup

VERSION = os.getenv('CI_COMMIT_TAG', '0.0.1')

setup(
    name='collectiveai-commons',
    packages=find_packages(),
    version=VERSION,
    description='{collectiveai} commons libraries.',
    author='collective.ai',
    author_email='team.collective.ai@gmail.com',
    url='https://gitlab.com/collective.ai/collectiveai-commons',
    install_requires=[
        'google-api-python-client==1.12.1',
        'google-auth-httplib2==0.0.4',
        'google-auth-oauthlib==0.4.1',
        'gspread==3.6.0',
        'gspread_formatting==0.3.3',
        'google-cloud-language==1.3.0',
        'pymongo==3.11.0',
        'pandas==1.1.3'
    ],
    package_data={'': ['*.yml', '*.yaml']},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3"
    ]
)
